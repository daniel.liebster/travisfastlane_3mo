# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#

require 'fileutils'

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

before_all do

  # Check if keychain exists and remove if nessecary
  if File.exists?(File.expand_path "~/Library/Keychains/#{ENV["MATCH_KEYCHAIN_NAME"]}-db")
    puts "Deleting Keychain: ~/Library/Keychains/#{ENV["MATCH_KEYCHAIN_NAME"]}-db"
    path = File.expand_path "~/Library/Keychains/#{ENV["MATCH_KEYCHAIN_NAME"]}-db"
    system 'security','delete-keychain', path
  end

  create_keychain(
    name: ENV["MATCH_KEYCHAIN_NAME"],
    default_keychain: true,
    unlock: true,
    timeout: 3600,
    add_to_search_list: true,
    password: ENV["MATCH_KEYCHAIN_PASSWORD"]
  )

  unlock_keychain(
    path: ENV["MATCH_KEYCHAIN_NAME"],
    add_to_search_list: true,
    password: ENV['MATCH_KEYCHAIN_PASSWORD']
  )
end

after_all do |lane|
  # This block is called, only if the executed lane was successful
  if ENV["SLACK_URL"]
    ENV["BUILD_NUMBER"] = get_info_plist_value(path: "ios/xxScreenersTV-tvOS/Info.plist", key: "CFBundleVersion")
    slack(
      message: nil,
      success: true,
      attachment_properties: {
       fields: [
           {
               title: "Build number",
               value: ENV["BUILD_NUMBER"],
           }
       ]
      }
    )
  end
end

lane :stage do
  begin
    # Get and format all the commits since the last version bump commit.
    notes = last_git_commit[:message] + "\nBuilding(Uploaded automatically via fastlane)"

    sync_code_signing(
      type: "adhoc",                             # adhoc or appstore
      keychain_name: ENV["MATCH_KEYCHAIN_NAME"],
      keychain_password: ENV["MATCH_KEYCHAIN_PASSWORD"]
    )

    # Fix to allow keychain to sign app without asking for password
    path = File.expand_path "~/Library/Keychains/#{ENV["MATCH_KEYCHAIN_NAME"]}-db"
    pass = ENV['MATCH_KEYCHAIN_PASSWORD']
    system 'security','set-key-partition-list','-S','apple-tool:,apple:','-s','-k', pass, path

    # debugging env on travis



    system 'yarn'
    # debugging xcode build command
    # system 'xcodebuild','clean','archive','-project','../ios/xxScreenersTV.xcodeproj','-scheme','xxScreenersTV-tvOS','-destination','generic/platform=tvOS','-archivePath','../ios/build/screeners.xcarchive','PROVISIONING_PROFILE=1136205c-0e7c-4c32-aaa7-b83efc2e8e84','PROVISIONING_PROFILE_SPECIFIER=Screeners_tvOS_AdHoc'

    
    # clean_build_artifacts

    # commit_version_bump(
    #   message: "Deployed new build #{lane_context[SharedValues::BUILD_NUMBER]}"
    # )

    # push_to_git_remote
  rescue => exception
    if ENV["SLACK_URL"]
      slack(
        message: exception.to_s,
        success: false
      )
    end
    UI.user_error!(exception)
  end
end

lane :prod do
  begin
    # Get and format all the commits since the last version bump commit.
    notes = last_git_commit[:message] + "\n(Uploaded automatically via fastlane)"

    sync_code_signing(
      type: "appstore",                             # adhoc or appstore
      keychain_name: ENV["MATCH_KEYCHAIN_NAME"],
      keychain_password: ENV["MATCH_KEYCHAIN_PASSWORD"]
    )

    # Fix to allow keychain to sign app without asking for password
    path = File.expand_path "~/Library/Keychains/#{ENV["MATCH_KEYCHAIN_NAME"]}-db"
    pass = ENV['MATCH_KEYCHAIN_PASSWORD']
    system 'security','set-key-partition-list','-S','apple-tool:,apple:','-s','-k', pass, path
    system 'yarn'

    # clean_build_artifacts

    # commit_version_bump(
    #   message: "Deployed new build #{lane_context[SharedValues::BUILD_NUMBER]}"
    # )

    # push_to_git_remote

  rescue => exception
    if ENV["SLACK_URL"]
      slack(
        message: exception.to_s,
        success: false
      )
    end
    UI.user_error!(exception)
  end
end
